# $_FOR_ROCKCHIP_RBOX_$
LOCAL_PATH:= $(call my-dir)

common_src_files := \
	VolumeManager.cpp \
	CommandListener.cpp \
	VoldCommand.cpp \
	NetlinkManager.cpp \
	NetlinkHandler.cpp \
	Volume.cpp \
	DirectVolume.cpp \
	logwrapper.c \
	Process.cpp \
	Ext4.cpp \
	Fat.cpp \
	Ntfs.cpp \
	Loop.cpp \
	Devmapper.cpp \
	ResponseCode.cpp \
	Xwarp.cpp \
	cryptfs.c

# $_rbox_$_modify_$_huangyonglin: added external/e2fsprogs/lib for using blkidlib
common_c_includes := \
	$(KERNEL_HEADERS) \
	system/extras/ext4_utils \
	external/e2fsprogs/lib \
	external/openssl/include
# $_rbox_$_modify_$ end


common_shared_libraries := \
	libsysutils \
	libcutils \
	libdiskconfig \
	libhardware_legacy \
	libcrypto

include $(CLEAR_VARS)

LOCAL_MODULE := libvold

LOCAL_SRC_FILES := $(common_src_files)

LOCAL_C_INCLUDES := $(common_c_includes)

LOCAL_SHARED_LIBRARIES := $(common_shared_libraries)

LOCAL_STATIC_LIBRARIES := libfs_mgr

LOCAL_MODULE_TAGS := eng tests

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE:= vold

LOCAL_SRC_FILES := \
	main.cpp \
	$(common_src_files)

LOCAL_C_INCLUDES := $(common_c_includes)

LOCAL_CFLAGS := -Werror=format

LOCAL_CFLAGS:=-DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)
# $_rbox_$_modify_$_huangyonglin: added external/e2fsprogs/lib for using blkidlib
LOCAL_STATIC_LIBRARIES := \
	libext2fs \
	libext2_blkid  \
	libext2_uuid  \
	libfs_mgr	\
	libext2_profile 
# $_rbox_$_modify_$ end

LOCAL_SHARED_LIBRARIES := $(common_shared_libraries)

ifeq ($(strip $(BOARD_RADIO_DATAONLY)), true)
LOCAL_SRC_FILES := \
	MiscManager.cpp \
	Misc.cpp \
	G3Dev.cpp \
	$(LOCAL_SRC_FILES)
	
LOCAL_CFLAGS += -DUSE_USB_MODE_SWITCH
endif

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= vdc.c

LOCAL_MODULE:= vdc

LOCAL_C_INCLUDES := $(KERNEL_HEADERS)

LOCAL_CFLAGS := 

LOCAL_SHARED_LIBRARIES := libcutils

include $(BUILD_EXECUTABLE)
