/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <linux/wireless.h>

#include <openssl/evp.h>
#include <openssl/sha.h>

#define LOG_TAG "SoftapController"
#include <cutils/log.h>
#include <netutils/ifc.h>
#include <private/android_filesystem_config.h>
#include "wifi.h"

#include "SoftapController.h"
extern "C" int wifi_load_ap_driver();
extern "C" int wifi_unload_ap_driver();
#undef HAVE_HOSTAPD
static const char HOSTAPD_CONF_FILE[]    = "/data/misc/wifi/hostapd.conf";
static const char RALINK_AP_DAT[] = "/data/misc/wifi/RT2870AP.dat";

SoftapController_mt7601::SoftapController_mt7601() {
    mPid = 0;
    mSock = socket(AF_INET, SOCK_DGRAM, 0);
    if (mSock < 0)
        ALOGE("Failed to open socket");
    else
    	ALOGE("SoftapController to open socket success");   
}

SoftapController_mt7601::~SoftapController_mt7601() {
    if (mSock >= 0)
        close(mSock);
}

int SoftapController_mt7601::startSoftap() {
    pid_t pid = 1;
    int ret = 0;

	ALOGE("Softap already started army debug1");//armychen

    if (mPid) {
        ALOGE("Softap already started");
        return 0;
    }
    if (mSock < 0) {
        ALOGE("Softap startap - failed to open socket");
        return -1;
    }
#ifdef HAVE_HOSTAPD
    if ((pid = fork()) < 0) {
        ALOGE("fork failed (%s)", strerror(errno));
        return -1;
    }
#endif
 ALOGE("Softap already started army debug");//armychen

	ret  = wifi_load_ap_driver();
	sleep(3);
	if(ret < 0)
	{
			ALOGE("wifi_load_ap_driver *************************");
	}

    if (!pid) {
#ifdef HAVE_HOSTAPD
        ensure_entropy_file_exists();
        if (execl("/system/bin/hostapd", "/system/bin/hostapd",
                  "-e", WIFI_ENTROPY_FILE,
                  HOSTAPD_CONF_FILE, (char *) NULL)) {
            ALOGE("execl failed (%s)", strerror(errno));
        }
#endif
        ALOGE("Should never get here!");
        return -1;
    } else {
        //*mBuf = 0;
		//ret = execl("/system/bin/netcfg", "/system/bin/netcfg", "wlan0", "up",(char *) NULL);
		system("netcfg wlan0 up");
		ret = 0;
        //ret = setCommand(mIface, "AP_BSS_START");
        if (ret) {
            ALOGE("Softap startap - failed: %d", ret);
        }
        else {
        	mPid = pid;
        	ALOGD("Softap startap - Ok");
        	usleep(AP_BSS_START_DELAY);
    	}
    }
    //return ret;
        ALOGD("Woody Retrurn - Ok");
    return 0;

}

int SoftapController_mt7601::stopSoftap() {
    int ret;

    if (mPid == 0) {
        ALOGE("Softap already stopped");
        return 0;
    }

#ifdef HAVE_HOSTAPD
    ALOGD("Stopping Softap service");
    kill(mPid, SIGTERM);
    waitpid(mPid, NULL, 0);
#endif
    if (mSock < 0) {
        ALOGE("Softap stopap - failed to open socket");
        return -1;
    }
    //*mBuf = 0;
	//ret = execl("/system/bin/netcfg", "/system/bin/netcfg", "wlan0", "down",(char *) NULL);
    //ret = setCommand(mIface, "AP_BSS_STOP");
	//ret = execl("/system/bin/netcfg", "/system/bin/netcfg", "wlan0", "up",(char *) NULL);
    system("netcfg wlan0 down");

	wifi_unload_ap_driver();	

    ret = 0;
    mPid = 0;
    ALOGD("Softap service stopped: %d", ret);
    usleep(AP_BSS_STOP_DELAY);
    return ret;
}

bool SoftapController_mt7601::isSoftapStarted() {
	ALOGD("mPid =%d",mPid);
    return (mPid != 0 ? true : false);
}
#if 0
int SoftapController::addParam(int pos, const char *cmd, const char *arg)
{
    if (pos < 0)
        return pos;
	pos = 0;
    if ((unsigned)(pos + strlen(cmd) + strlen(arg) + 1) >= sizeof(mBuf)) {
        ALOGE("Command line is too big");
        return -1;
    }
    pos += sprintf(&mBuf[pos], "%s=%s", cmd, arg);
    return pos;
}
#endif
/*/mBuf
 * Arguments:
 *      argv[2] - wlan interface
 *      argv[3] - SSID
 *	argv[4] - Security
 *	argv[5] - Key
 *	argv[6] - Channel
 *	argv[7] - Preamble
 *	argv[8] - Max SCB
 */
int SoftapController_mt7601::setSoftap(int argc, char *argv[]) {
    char psk_str[2*SHA256_DIGEST_LENGTH+1];
    int ret = 0, i = 0, fd;
    char *ssid, *iface;

	ALOGD("Woody setSoftap- Ok");
    if (mSock < 0) {
        ALOGE("Softap set - failed to open socket");
        return -1;
    }
    if (argc < 4) {
        ALOGE("Softap set - missing arguments");
        return -1;
    }

    iface = argv[2];

#ifdef HAVE_HOSTAPD
    char *wbuf = NULL;
    char *fbuf = NULL;

    if (argc > 3) {
        ssid = argv[3];
    } else {
        ssid = (char *)"AndroidAP";
    }

    asprintf(&wbuf, "interface=%s\ndriver=nl80211\nctrl_interface="
            "/data/misc/wifi/hostapd\nssid=%s\nchannel=6\nieee80211n=1\n",
            iface, ssid);

    if (argc > 4) {
        if (!strcmp(argv[4], "wpa-psk")) {
            generatePsk(ssid, argv[5], psk_str);
            asprintf(&fbuf, "%swpa=1\nwpa_pairwise=TKIP CCMP\nwpa_psk=%s\n", wbuf, psk_str);
        } else if (!strcmp(argv[4], "wpa2-psk")) {
            generatePsk(ssid, argv[5], psk_str);
            asprintf(&fbuf, "%swpa=2\nrsn_pairwise=CCMP\nwpa_psk=%s\n", wbuf, psk_str);
        } else if (!strcmp(argv[4], "open")) {
            asprintf(&fbuf, "%s", wbuf);
        }
    } else {
        asprintf(&fbuf, "%s", wbuf);
    }

    fd = open(HOSTAPD_CONF_FILE, O_CREAT | O_TRUNC | O_WRONLY, 0660);
    if (fd < 0) {
        ALOGE("Cannot update \"%s\": %s", HOSTAPD_CONF_FILE, strerror(errno));
        free(wbuf);
        free(fbuf);
        return -1;
    }
    if (write(fd, fbuf, strlen(fbuf)) < 0) {
        ALOGE("Cannot write to \"%s\": %s", HOSTAPD_CONF_FILE, strerror(errno));
        ret = -1;
    }
    close(fd);
    free(wbuf);
    free(fbuf);

    /* Note: apparently open can fail to set permissions correctly at times */
    if (chmod(HOSTAPD_CONF_FILE, 0660) < 0) {
        ALOGE("Error changing permissions of %s to 0660: %s",
                HOSTAPD_CONF_FILE, strerror(errno));
        unlink(HOSTAPD_CONF_FILE);
        return -1;
    }

    if (chown(HOSTAPD_CONF_FILE, AID_SYSTEM, AID_WIFI) < 0) {
        ALOGE("Error changing group ownership of %s to %d: %s",
                HOSTAPD_CONF_FILE, AID_WIFI, strerror(errno));
        unlink(HOSTAPD_CONF_FILE);
        return -1;
    }

#else
    char *wbuf = NULL;
    char *fbuf = NULL;

	if (argc > 3) {
        ssid = argv[3];
    } else {
        ssid = (char *)"AndroidAP";
    }

    asprintf(&wbuf, "Default\nBssidNum=1\nWirelessMode=9\nCountryRegion=5\n"
            "Channel=6\nSSID=%s\nBeaconPeriod=100\nTxPower=100\nRTSThreshold=2347\nFragThreshold=2346\n"
			"TxBurst=1\nPktAggregate=0\nWmmCapable=1\n"
			"HT_RDG=1\nHT_EXTCHA=0\nHT_OpMode=0\nHT_MpduDensity=4\nHT_BW=0\nHT_BADecline=0\nHT_AutoBA=1\n"
			"HT_AMSDU=0\nHT_BAWinSize=64\nHT_GI=1\nHT_MCS=33\nHT_MIMOPSMode=3\nHT_DisallowTKIP=1\nHT_STBC=0\n",
            ssid);

    if (argc > 4) {
        if (!strcmp(argv[4], "wpa-psk")) {
            asprintf(&fbuf, "%sAuthMode=WPAPSK\nEncrypType=TKIPAES\nWPAPSK=%s\nDefaultKeyID=2\n", wbuf, argv[5]);
        } else if (!strcmp(argv[4], "wpa2-psk")) {
            asprintf(&fbuf, "%sAuthMode=WPA2PSK\nEncrypType=AES\nWPAPSK=%s\nDefaultKeyID=2\n", wbuf, argv[5]);
        } else if (!strcmp(argv[4], "open")) {
            asprintf(&fbuf, "%sAuthMode=OPEN\nEncrypType=NONE\nDefaultKeyID=1\n", wbuf);
        }
    } else {
        asprintf(&fbuf, "%sAuthMode=OPEN\nEncrypType=NONE\nDefaultKeyID=1\n", wbuf);
    }

    fd = open(RALINK_AP_DAT, O_CREAT | O_TRUNC | O_WRONLY, 0660);
    if (fd < 0) {
        ALOGE("Cannot update \"%s\": %s", RALINK_AP_DAT, strerror(errno));
        free(wbuf);
        free(fbuf);
        return -1;
    }
    if (write(fd, fbuf, strlen(fbuf)) < 0) {
        ALOGE("Cannot write to \"%s\": %s", RALINK_AP_DAT, strerror(errno));
        ret = -1;
    }
    close(fd);
    free(wbuf);
    free(fbuf);

    /* Note: apparently open can fail to set permissions correctly at times */
    if (chmod(RALINK_AP_DAT, 0660) < 0) {
        ALOGE("Error changing permissions of %s to 0660: %s",
                RALINK_AP_DAT, strerror(errno));
        unlink(RALINK_AP_DAT);
        return -1;
    }

    if (chown(RALINK_AP_DAT, AID_SYSTEM, AID_WIFI) < 0) {
        ALOGE("Error changing group ownership of %s to %d: %s",
                RALINK_AP_DAT, AID_WIFI, strerror(errno));
        unlink(RALINK_AP_DAT);
        return -1;
    }

	usleep(AP_SET_CFG_DELAY);

#if 0

 /* Create command line */
	*mBuf = 0;

	char *key_buf;
	int key_len = 0;
    if (argc > 4) {
        ssid = argv[4];
    } else {
        ssid = (char *)"AndroidAP";
    }
    i = addParam(i, "SSID", ssid);

	if (argc > 5){
		 if (!strcmp(argv[5], "wpa-psk")) {
			i = addParam(i, "AuthMode", "WPAPSK");
			setCommand(iface, "set");
			*mBuf = 0;
			i = addParam(i, "EncrypType", "TKIPAES");
			setCommand(iface, "set");
            *mBuf = 0;
			i = addParam(i, "DefaultKeyID", "2");
            setCommand(iface, "set");
            *mBuf = 0;
			key_len = strlen(argv[6]);
			key_buf = (char *)malloc(key_len);
			memset(key_buf, 0, key_len);
			memcpy(key_buf, argv[6], key_len);
			key_buf[key_len] = '\0';
			ALOGE("key_buf = %s argv[6] = %s key_len = %d",
                key_buf, argv[6], key_len);
			i = addParam(i, "WPAPSK", key_buf);
			ALOGE("mBuf = %s", mBuf);
            setCommand(iface, "set");
			free(key_buf);
            *mBuf = 0;
			usleep(AP_SET_CFG_DELAY);
        } else if (!strcmp(argv[5], "wpa2-psk")) {
			i = addParam(i, "AuthMode", "WPA2PSK");
            setCommand(iface, "set");
            *mBuf = 0;
            i = addParam(i, "EncrypType", "AES");
            setCommand(iface, "set");
            *mBuf = 0;
			i = addParam(i, "DefaultKeyID", "2");
            setCommand(iface, "set");
            *mBuf = 0;
			key_len = strlen(argv[6]);
			key_buf = (char *)malloc(key_len);
			memset(key_buf, 0, key_len);
            memcpy(key_buf, argv[6], key_len);
			key_buf[key_len] = '\0';
            i = addParam(i, "WPAPSK", key_buf);
			ALOGE("key_buf = %s argv[6] = %s key_len = %d",
                key_buf, argv[6], key_len);
			ALOGE("mBuf = %s", mBuf);
            setCommand(iface, "set");
			free(key_buf);
            *mBuf = 0;
			usleep(AP_SET_CFG_DELAY);
        } else if (!strcmp(argv[5], "open")) {
            i = addParam(i, "AuthMode", "OPEN");
            setCommand(iface, "set");
            *mBuf = 0;
            i = addParam(i, "EncrypType", "NONE");
            setCommand(iface, "set");
            *mBuf = 0;
        }
	} else {
		i = addParam(i, "AuthMode", "OPEN");
        setCommand(iface, "set");
        *mBuf = 0;
        i = addParam(i, "EncrypType", "NONE");
        setCommand(iface, "set");
        *mBuf = 0;
	}

    if (argc > 7) {
		i = addParam(i, "Channel", argv[7]);
        setCommand(iface, "set");
        *mBuf = 0;
    } else {
        i = addParam(i, "Channel", "6");
		setCommand(iface, "set");
        *mBuf = 0;
    }
    if ((i < 0) || ((unsigned)(i + 4) >= sizeof(mBuf))) {
        ALOGE("Softap set - command is too big");
        return i;
    }

	*mBuf = 0;
	i = addParam(i, "SSID", ssid);
	ret = setCommand(iface, "set");
    *mBuf = 0;
    /* system("iwpriv eth0 WL_AP_CFG ASCII_CMD=AP_CFG,SSID=\"AndroidAP\",SEC=\"open\",KEY=12345,CHANNEL=1,PREAMBLE=0,MAX_SCB=8,END"); */
    //ret = setCommand(iface, "AP_SET_CFG");
    if (ret) {
        ALOGE("Softap set - failed: %d", ret);
    }
    else {
        ALOGD("Softap set - Ok");
        usleep(AP_SET_CFG_DELAY);
    }
#endif
#endif
    return ret;
}

void SoftapController_mt7601::generatePsk(char *ssid, char *passphrase, char *psk_str) {
    unsigned char psk[SHA256_DIGEST_LENGTH];
    int j;
    // Use the PKCS#5 PBKDF2 with 4096 iterations
    PKCS5_PBKDF2_HMAC_SHA1(passphrase, strlen(passphrase),
            reinterpret_cast<const unsigned char *>(ssid), strlen(ssid),
            4096, SHA256_DIGEST_LENGTH, psk);
    for (j=0; j < SHA256_DIGEST_LENGTH; j++) {
        sprintf(&psk_str[j<<1], "%02x", psk[j]);
    }
    psk_str[j<<1] = '\0';
}


/*
 * Arguments:
 *	argv[2] - interface name
 *	argv[3] - AP or STA
 */
int SoftapController_mt7601::fwReloadSoftap(int argc, char *argv[])
{
    int ret, i = 0;
    char *iface;
    char *fwpath;

	ALOGE("Softap fwrealod 7601###################################");

    if (mSock < 0) {
        ALOGE("Softap fwrealod - failed to open socket");
        return -1;
    }
    if (argc < 4) {
        ALOGE("Softap fwreload - missing arguments");
        return -1;
    }

    iface = argv[2];

    if (strcmp(argv[3], "AP") == 0) {
        fwpath = (char *)wifi_get_fw_path(WIFI_GET_FW_PATH_AP);
    } else if (strcmp(argv[3], "P2P") == 0) {
        fwpath = (char *)wifi_get_fw_path(WIFI_GET_FW_PATH_P2P);
    } else {
        fwpath = (char *)wifi_get_fw_path(WIFI_GET_FW_PATH_STA);
    }
    if (!fwpath)
        return -1;
#if 1
	//
	ret = wifi_change_fw_path((const char *)fwpath);
#else
    sprintf(mBuf, "FW_PATH=%s", fwpath);
    ret = setCommand(iface, "WL_FW_RELOAD");
#endif
    if (ret) {
        ALOGE("Softap fwReload - failed: %d", ret);
    }
    else {
        ALOGD("Softap fwReload - Ok");
    }
    return ret;
}

int SoftapController_mt7601::clientsSoftap(char **retbuf)
{
#if 0
    int ret;

    if (mSock < 0) {
        ALOGE("Softap clients - failed to open socket");
        return -1;
    }
    //*mBuf = 0;
    ret = setCommand(mIface, "AP_GET_STA_LIST", SOFTAP_MAX_BUFFER_SIZE);
    if (ret) {
        ALOGE("Softap clients - failed: %d", ret);
    } else {
        asprintf(retbuf, "Softap clients:%s", mBuf);
        ALOGD("Softap clients:%s", mBuf);
    }
#endif
	return 0;
}
