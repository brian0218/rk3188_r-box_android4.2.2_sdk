/*****************************************************************************/
/*                                                                           */
/*  Name:          bte_version.c                                             */
/*                                                                           */
/*  Description:                                                             */
/*      BTE Version string declaration.                                      */
/*                                                                           */
/*  Copyright (c) 2001 - 2004, WIDCOMM Inc., All Rights Reserved.            */
/*  WIDCOMM Bluetooth Core. Proprietary and confidential.                    */
/*****************************************************************************/

#include "bt_types.h"

#ifdef BLUETOOTH_USE_BPLUS
const UINT8 bte_version_string[] = "BCM1200_PI_10.3.20.52";
const UINT8 btif_version_string[] = "BPLUS-420-10_00.10-lite";
#else
const UINT8 bte_version_string[] = "BCM1200_PI_10.3.20.33";
#endif

