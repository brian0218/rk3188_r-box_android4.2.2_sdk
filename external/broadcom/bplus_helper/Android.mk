LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

TARGET_PRELINK_MODULE := false

LOCAL_SRC_FILES := bplus_helper.c

LOCAL_SHARED_LIBRARIES := \
        libcutils \
        libutils \

LOCAL_CFLAGS := 

LOCAL_MODULE := bplus_helper
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

