#include "bplus_customize.h"

#include <stdio.h>

#include <cutils/properties.h>
#define LOG_TAG "bluetooth_cust"
#include <cutils/log.h>

#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

char * GetDisplayLable()
{
    char lable[128] = "";

    // ro.product.model
    property_get("ro.product.model", lable, "RK_BT_4.0");;
    ALOGD("%s: label = %s\n", __FUNCTION__, lable);
	return lable;
}

char * GetFwFullDir()
{
	char *FwFullDir = "/vendor/firmware/";

    ALOGD("%s: %s\n", __FUNCTION__, FwFullDir);
	return FwFullDir;
}

static int bt_get_chipname(char* name, int len)
{
    int ret = -1; 
    int fd = -1; 
    int sz = 0;
    char* rfkill_name_path = NULL;
    
    asprintf(&rfkill_name_path, "/sys/class/rfkill/rfkill0/name");

    fd = open(rfkill_name_path, O_RDONLY);
    if (fd < 0) {
        ALOGE("open(%s) failed: %s (%d)", rfkill_name_path, strerror(errno),
             errno);
        goto out;
    }   
    
    sz = read(fd, name, len);
    if (sz <= 0) {
        ALOGE("read(%s) failed: %s (%d)", rfkill_name_path, strerror(errno),
             errno);
        goto out;
    }   
    name[sz] = '\0';
    if (name[sz-1]=='\n')
        name[sz-1] = '\0';

    ret = 0;

out:
    if (fd >= 0) close(fd);
    return ret;
}

char * GetFwFilename()
{
	char FwFilename[128];

    char bt_chip[64] = "";
    
    if (bt_get_chipname(bt_chip, 63) == 0)
    {    
        ALOGI("BT module name is: %s\n", bt_chip);
        if (!strcmp(bt_chip, "rk903_26M"))
            sprintf(FwFilename, "%s", "rk903_26M.hcd");
        else if (!strcmp(bt_chip, "rk903"))
            sprintf(FwFilename, "%s", "rk903.hcd");
        else if (!strcmp(bt_chip, "nh660"))
            sprintf(FwFilename, "%s", "nh660.hcd");
        else if (!strcmp(bt_chip, "bcm4329"))
            sprintf(FwFilename, "%s", "bcm4329_samsung.hcd");
        else if (!strcmp(bt_chip, "mv8787"))
            sprintf(FwFilename, "%s", "mv8787.hcd");
        else if (!strcmp(bt_chip, "ap6210"))
            sprintf(FwFilename, "%s", "bcm20710a1.hcd");
        else if (!strcmp(bt_chip, "ap6330") 
            || !strcmp(bt_chip, "ap6493"))
            sprintf(FwFilename, "%s", "bcm40183b2.hcd");
        else if (!strcmp(bt_chip, "ap6476"))
            sprintf(FwFilename, "%s", "bcm2076b1.hcd");
        else 
            sprintf(FwFilename, "%s", bt_chip);
    }    

    ALOGD("%s: FwFilename = %s\n", __FUNCTION__, FwFilename);
	return FwFilename;
}

void NotifyChipID( char * chip_name)
{
/********************DO NOT REMOVE THIS INFORMATION*****************************************************/
    ALOGD("BPLUS SDK Version: %s \n",version);
/********************DO NOT REMOVE THIS INFORMATION*****************************************************/

    ALOGD("%s: chipname = %s\n", __FUNCTION__, chip_name);
}

