/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;

import com.broadcom.app.leexplorer.DataUtils;
import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;
import com.broadcom.bt.gatt.BluetoothGattCharacteristic;

public class DefaultFragment extends ValueFragment implements OnItemSelectedListener {
    private byte[] mValue = null;
    private int mValueType = TYPE_HEX;
    private boolean mReadonly = false;

    private static final int TYPE_HEX = 0;
    private static final int TYPE_UINT8 = 1;
    private static final int TYPE_UINT16 = 2;
    private static final int TYPE_STRING = 3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_default, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Spinner valueType = (Spinner)findViewById(R.id.spinTyoe);
        valueType.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        mValueType = pos;
        setKeyboard();
        parseValue();
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    @Override
    public void setReadonly(boolean readonly) {
        mReadonly = readonly;
        setKeyboard();
    }

    @Override
    public void setValue(byte[] value) {
        mValue = value;
        parseValue();
    }

    @Override
    public boolean assignValue(BluetoothGattCharacteristic characteristic) {
        EditText editValue = (EditText)findViewById(R.id.editValue);
        String valueString = editValue.getText().toString();
        if (valueString == null || valueString.length() == 0) return false;

        switch (mValueType) {
        case TYPE_HEX:
            {
                byte[] value = parseHex(valueString);
                if (value.length == 0) return false;
                characteristic.setValue(value);
                break;
            }

            case TYPE_UINT8:
            {
                Integer value = Integer.parseInt(editValue.getText().toString());
                if (value == null) return false;
                characteristic.setValue(value, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                break;
            }

            case TYPE_UINT16:
            {
                Integer value = Integer.parseInt(editValue.getText().toString());
                if (value == null) return false;
                characteristic.setValue(value, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
                break;
            }

            case TYPE_STRING:
            {
                characteristic.setValue(valueString);
                break;
            }
        }

        return true;
    }

    private void parseValue() {
        EditText editValue = (EditText)findViewById(R.id.editValue);
        if (mValue == null || editValue == null) return;

        switch (mValueType) {
            case TYPE_STRING:
            {
                String value = new String(mValue);
                editValue.setText(value);
                break;
            }

            case TYPE_UINT8:
                if (mValue.length >= 1) {
                    Integer value = DataUtils.unsignedByteToInt(mValue[0]);
                    editValue.setText(value.toString());
                }
                break;

            case TYPE_UINT16:
                if (mValue.length >= 2) {
                    Integer value = DataUtils.unsignedBytesToInt(mValue[0], mValue[1]);
                    editValue.setText(value.toString());
                }
                break;

            case TYPE_HEX:
            default:
                editValue.setText(valueToHex(mValue));
                break;
        }
    }

    private void setKeyboard()
    {
        EditText editValue = (EditText)findViewById(R.id.editValue);
        if (mReadonly) {
            editValue.setInputType(InputType.TYPE_NULL);
        } else {
            switch (mValueType) {
            case TYPE_UINT8:
            case TYPE_UINT16:
                editValue.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;

            case TYPE_STRING:
            case TYPE_HEX:
            default:
                editValue.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            }
        }
    }

    private String valueToHex(byte[] value) {
        String hex = "";
        for (byte bb : value) hex += String.format("%02x ", bb);
        return hex.subSequence(0, hex.length()-1).toString();  // Remove trailing space...

    }

    private byte[] parseHex(String hexString) {
        hexString = hexString.replaceAll("\\s", "").toUpperCase();
        String filtered = new String();
        for(int i = 0; i != hexString.length(); ++i) {
            if (hexVal(hexString.charAt(i)) != -1)
                filtered += hexString.charAt(i);
        }

        if (filtered.length() % 2 != 0) {
            char last = filtered.charAt(filtered.length() - 1);
            filtered = filtered.substring(0, filtered.length() - 1) + '0' + last;
        }

        return hexStringToByteArray(filtered);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private int hexVal(char ch) {
        return Character.digit(ch, 16);
    }
}
