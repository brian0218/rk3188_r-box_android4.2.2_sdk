/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.broadcom.app.leexplorer.DataUtils;
import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;

public class CscFeatureFragment extends ValueFragment {
    private static final char LINE_FEED = 0x0A;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_simplestring, container, false);
    }

    @Override
    public void setValue(byte[] value) {
        StringBuilder sb = new StringBuilder();
        String[] featureStrings = getStringArray(R.array.csc_feature);

        int features = DataUtils.unsignedBytesToInt(value[0], value[1]);

        for (int i=0; i != featureStrings.length; ++i) {
            if (((1 << i) & features) != 0) {
                sb.append(featureStrings[i]);
                sb.append(LINE_FEED);
            }
        }

        if (sb.length() == 0) sb.append(getString(R.string.unknown_value));

        setVisible(R.id.textTitle);
        setText(R.id.textValue, sb.toString());
    }
}
