/************************************************************************************
 *
 *  Copyright (C) 2009-2011 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ************************************************************************************/
package com.broadcom.bt.ProximityMonitor;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import com.broadcom.bt.ProximityMonitor.R;
//import com.google.android.maps.GeoPoint;
//import com.google.android.maps.MapController;
//import com.google.android.maps.MapView;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import java.util.Date;
import java.text.DateFormat;

import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.net.http.SslError;
import android.graphics.Bitmap;
import android.os.Message;
import android.view.KeyEvent;

public class ProximityMapActivity extends Activity {

    public final static String TAG = "ConfigureProximityReporterActivity";

	private final static int DIALOG_NAME = 1;

    String address;
    ProximityService service;

    ProximityReporter proximityReporter = null;

    private ServiceConnection onService = null;
	
	private boolean needToLoadMap = false;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        address = getIntent().getExtras().getString("address");

        onService = new ServiceConnection() {
            public void onServiceConnected(ComponentName className,
                    IBinder rawBinder) {
                service = ((ProximityService.LocalBinder) rawBinder).getService();
                proximityReporter = service.getProximityReporter(address);
				if (proximityReporter != null) {
					init();
			    } else {
				    // we should never get here, unless the device was just unpaired and removed from the database
					Toast.makeText(ProximityMapActivity.this, "Device " + address + " not found.", Toast.LENGTH_LONG).show();
					finish();
				}
            }

            public void onServiceDisconnected(ComponentName classname) {
                service = null;
            }
        };

        // start service, if not already running (but it is)
        startService(new Intent(this, ProximityService.class));

        Intent bindIntent = new Intent(this, ProximityService.class);
        bindService(bindIntent, onService, Context.BIND_AUTO_CREATE);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(onService);
        service = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_close_enter, R.anim.activity_close_exit);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_NAME:
            final EditText input = new EditText(this);
            input.setText(proximityReporter.nickname);
            return new AlertDialog.Builder(
                    ProximityMapActivity.this)
                    .setTitle(getString(R.string.set_nickname_dialog_title))
					.setView(input)
					.setPositiveButton(
                            android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
									String s = input.getText().toString();
									if (s == null || s.length() == 0)
										return; // no new name
                                    proximityReporter.nickname = s;
									((TextView) findViewById(R.id.name_1)).setText(proximityReporter.nickname);
									((TextView) findViewById(R.id.name_2)).setText(proximityReporter.bluetoothDeviceName);
                                    service.update(proximityReporter, true, true);
                                }
                            }).setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                }
                            }).create();
        default:
            return null;
        }
    }

    private void init() {
        this.setContentView(R.layout.map);
        
		if (proximityReporter.nickname == null || proximityReporter.nickname.length() == 0) {
            ((TextView) findViewById(R.id.name_1)).setText(proximityReporter.bluetoothDeviceName);
			((TextView) findViewById(R.id.name_2)).setText(getString(R.string.set_nickname));
		} else {
			((TextView) findViewById(R.id.name_1)).setText(proximityReporter.nickname);
            ((TextView) findViewById(R.id.name_2)).setText(proximityReporter.bluetoothDeviceName);
		}
		
        findViewById(R.id.name_1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
				showDialog(DIALOG_NAME);
            }
        });
		
        findViewById(R.id.name_2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
				showDialog(DIALOG_NAME);
            }
        });
		
		if (proximityReporter.timestamp != 0) {
			TextView timestampText = (TextView) findViewById(R.id.timestamp_text);
			DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
			timestampText.setText(
				getString(R.string.map_timestamp_prefix)
				+ df.format(new Date(proximityReporter.timestamp))
				+ getString(R.string.map_timestamp_suffix));
		}
		
		final WebView webView = (WebView) findViewById(R.id.web);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient() {  
		
			public void onLoadResource(WebView view, String url) {
				Log.d(TAG, "onLoadResource / " + url);
			}
			public void onPageFinished(WebView view, String url) {
				Log.d(TAG, "onPageFinished / " + url);
				if (needToLoadMap) {
				    needToLoadMap = false;
					Log.d(TAG, "loadUrl - lat="+proximityReporter.latitude+"&lon="+proximityReporter.longitude);
					//	webView.loadUrl("file:///android_asset/map.html?lat=33.023374&lon=-117.081981");
				    webView.loadUrl("file:///android_asset/map.html?lat="+proximityReporter.latitude+"&lon="+proximityReporter.longitude);
				}
			}
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				Log.d(TAG, "onPageStarted / " + url);
			}
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				Log.d(TAG, "onReceivedError / " + errorCode + " / " + description);
			}
			public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
				Log.d(TAG, "onReceivedHttpAuthRequest / " + realm);
			}
			public void onReceivedLoginRequest(WebView view, String realm, String account, String args) {
				Log.d(TAG, "onReceivedLoginRequest / " + realm);
			}
			public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
				Log.d(TAG, "onReceivedSslError");
			}
			public void onScaleChanged(WebView view, float oldScale, float newScale) {
				Log.d(TAG, "onScaleChanged / " + newScale);
			}
			public void onTooManyRedirects(WebView view, Message cancelMsg, Message continueMsg) {
				Log.d(TAG, "onTooManyRedirects");
			}
			public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
				Log.d(TAG, "onUnhandledKeyEvent");
			}

		/*
			@Override  
			public boolean shouldOverrideUrlLoading(WebView view, String url) {  
				view.loadUrl(url);
				return true;
			}  
			*/
		});
		if (proximityReporter.latitude == 0 && proximityReporter.longitude == 0) {
		    webView.loadUrl("file:///android_asset/no_location.html");
		} else if (isDataConnectionAvailable()) {
		    needToLoadMap = true;
		    webView.loadUrl("file:///android_asset/loading.html");
		} else {
		    webView.loadUrl("file:///android_asset/no_data.html");
		}
    }
    
	// TODO: move to a util class?
	private boolean isDataConnectionAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm != null) {
		    NetworkInfo[] networks = cm.getAllNetworkInfo();
			for (NetworkInfo network: networks) {
			    if (network.getState() == NetworkInfo.State.CONNECTED) {
				    return true;
				}
			}
		}
		return false;
	}
}
